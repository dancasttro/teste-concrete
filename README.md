# Teste Concrete Solutions

## Tecnologias usadas

**Angular**,
**npm**,
**Gulp**,
**Bower**,
**SASS**,
**ESLint**,
**Browsersync**,
**Normalize**,
**Autoprefixer**,
**UI-Router**

## Instalação

### Dependências

Precisa instalar `npm` e `bower`.

Instalando `Gulp`

```sh
$ npm install -g gulp
```

Instalando `Bower`

```sh
$ npm install -g bower
```

### Instalação

Clone repositório do Gitlab:

```sh
$ git clone https://gitlab.com/dancasttro/teste-concrete.git
```

Entre no diretório `teste-concrete` e instale as dependências `npm`:

```sh
$ cd teste-concrete
$ npm install
```

Agora, instale as dependências do `bower`:

```sh
$ bower install
```

Pronto! Agora é so rodar o comando:

```sh
$ gulp serve
```

Para gerar um build é so rodar o comando:

```sh
$ gulp default
```