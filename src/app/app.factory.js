angular
  .module('app')
  .factory('ApiGithub', ApiGithub);

function ApiGithub(BASE_URL, $http) {

  var service = {
    getUser: getUser,
    getUserRepos: getUserRepos,
    getRepoDetails: getRepoDetails
  };

  return service;

  function getUser(id) {
    return $http.get(BASE_URL + /users/ + id)
  }

  function getUserRepos(id) {
    return $http.get(BASE_URL + '/users/' + id + '/repos')
  }

  function getRepoDetails(id) {
    return $http.get(BASE_URL + '/repos/' + id)
  }
}
