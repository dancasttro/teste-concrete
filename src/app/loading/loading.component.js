angular.module('app').component('loading', {
  controller: LoadingController,
  controllerAs: 'vm',
  templateUrl: 'app/loading/loading.view.html'
});

/** @ngInject */
function LoadingController() {
  var vm = this;
  
  vm.load = load;

  function load(val) {
    if (val) {
      vm.loading = true;
    }
    else {
      vm.loading = false;
    }
  }
}
