angular.module('app').config(configApp);

  /** @ngInject */
  function configApp($locationProvider, $logProvider) {
    
    $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');

    $logProvider.debugEnabled(false);

}