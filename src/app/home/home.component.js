angular.module('app').component('home', {
  controller: HomeController,
  controllerAs: 'vm',
  templateUrl: 'app/home/home.view.html'
});

/** @ngInject */
function HomeController(ApiGithub) {
  var vm = this;

  vm.userData = [];
  vm.userRepos = [];
  vm.searchUser = searchUser;
  vm.orderBy = orderBy;
  vm.boxUser = false;
  
  function searchUser() {
    ApiGithub.getUser(vm.username)
    .then(getUserSuccess)
    .then(getUserRepos)
    .catch(handleError);
  }
  
  function getUserRepos() {
    vm.load = true;
    ApiGithub.getUserRepos(vm.username)
    .then(getReposSuccess)
    .catch(handleError);
  }
  
  function getReposSuccess(response) {
    vm.userRepo = response.data;
    vm.boxUser = true;
    vm.load = false;
  }

  function getUserSuccess(response) {
    vm.userData = response.data;
    return response;
  }

  function orderBy(field) {
    vm.criterionSorting = field;
    vm.directionSorting = !vm.directionSorting;
  }

  function handleError(error) {
    console.log(error);
  }

}