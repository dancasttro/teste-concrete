
angular.module('app').config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('home', {
        url: '/',
        component: 'home'
      })
      .state('loading', {
        url: '/',
        template: '<loading></loading>'
      })
      .state('repo', {
        url: '/repo/:id',
        component: 'repo'
      });

    $urlRouterProvider.otherwise('/');
  }