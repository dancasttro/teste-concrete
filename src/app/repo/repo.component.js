angular.module('app').component('repo', {
  controller: RepoController,
  controllerAs: 'vm',
  templateUrl: 'app/repo/repo.view.html'
});

/** @ngInject */
function RepoController($stateParams, ApiGithub, $window) {
var vm = this;

vm.goBack = goBack;

vm.id = $stateParams.id;

function init() {
  ApiGithub
    .getRepoDetails(vm.id)
    .then(getRepoSuccess)
    .catch(handleError);
}

function getRepoSuccess(response) {
  vm.repoData = response.data;
}

function goBack() {
  $window.history.back();
}

function handleError(error) {
  console.log(error);
}

init();

}
